# Money Tracker Flutter

Flutter application for cost accounting

Use stack
- db: firebase
- chart: charts_flutter: ^0.9.0
- colorpicker: flutter_colorpicker: ^0.3.5

[Демо (YouTube)](https://youtu.be/0dGcIGbCjD8)

<img src="https://gitlab.com/dshevchenkoo/money-tracker-flutter/-/raw/master/screens/first_screen.jpeg" width="200"/>
<img src="https://gitlab.com/dshevchenkoo/money-tracker-flutter/-/raw/master/screens/main_screen.jpeg" width="200"/>​​
<img src="https://gitlab.com/dshevchenkoo/money-tracker-flutter/-/raw/master/screens/add_category_screen.jpeg" width="200"/>
<img src="https://gitlab.com/dshevchenkoo/money-tracker-flutter/-/raw/master/screens/add_rate_screen.jpeg" width="200"/>​​
