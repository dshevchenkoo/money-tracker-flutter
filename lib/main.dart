import 'package:diplom_money_tracker/accounting/rate/rate_view.dart';
import 'package:diplom_money_tracker/app_view.dart';
import 'package:diplom_money_tracker/auth/sign_in_view.dart';
import 'package:flutter/material.dart';
import 'accounting/accounting_view.dart';
import 'package:provider/provider.dart';
import 'auth/provider/auth_provider.dart';
import 'auth/sign_up_view.dart';
import 'package:firebase_core/firebase_core.dart';
import 'auth/security.dart';
import 'accounting/provider/accounting_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context) => AuthProvider()),
    ChangeNotifierProvider(create: (context) => AccountingProvider()),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Money Tracker',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.purple,
      ),
      initialRoute: '/security',
      routes: {
        '/security': (context) => Security(),
        '/login/sign-in': (context) => SignIn(),
        '/login/sign-up': (context) => SignUp(),
        '/accounting': (context) => AppView(),
        '/rate': (context) => RateView(),
      },
    );
  }
}
