import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddRate extends StatefulWidget {
  AddRate({Key key, this.id}) : super(key: key);
  final id;

  @override
  _AddRateState createState() => _AddRateState();
}

class _AddRateState extends State<AddRate> {
  @override
  Widget build(BuildContext context) {
    TextEditingController moneyController = TextEditingController();
    TextEditingController commentController = TextEditingController();

    bool _validMoney = true;
    Color myColor = Color.fromRGBO(144, 23, 255, 100);
    String _id = widget.id;
    bool isNumeric(String s) {
      if (s == null) {
        return false;
      }
      return double.tryParse(s) != null;
    }

    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      title: Consumer<AccountingProvider>(
          builder: (context, accountingProvider, child) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('Добавить расход'),
                  SizedBox(
                    width: 25,
                  ),
                  Text(
                    '${accountingProvider.getShortDate()}',
                    style:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                  )
                ],
              )),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            TextField(
              controller: moneyController,
              decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: myColor),
                      borderRadius: BorderRadius.circular(15)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: myColor),
                      borderRadius: BorderRadius.circular(15)),
                  errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                      borderRadius: BorderRadius.circular(15)),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                      borderRadius: BorderRadius.circular(15)),
                  errorText: !_validMoney ? 'Сумма' : null,
                  labelText: 'Сумма'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: commentController,
              decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: myColor),
                      borderRadius: BorderRadius.circular(15)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: myColor),
                      borderRadius: BorderRadius.circular(15)),
                  labelText: 'Комментарий'),
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 45,
              child: Consumer<AccountingProvider>(
                  builder: (context, accountingProvider, child) => RaisedButton(
                        color: Color.fromRGBO(144, 23, 255, 100),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          'Добавить',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.normal),
                        ),
                        onPressed: () {
                          setState(() {
                            moneyController.text.isEmpty
                                ? _validMoney = false
                                : _validMoney = true;
                            !isNumeric(moneyController.text)
                                ? _validMoney = false
                                : _validMoney = true;
                          });

                          if (!_validMoney) {
                            return;
                          }

                          accountingProvider.addRate(_id, moneyController.text,
                              commentController.text);

                          Navigator.of(context).pop();
                        },
                      )),
            ),
            CupertinoButton(
                child: Text(
                  'Отмена',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
      ),
    );
  }
}
