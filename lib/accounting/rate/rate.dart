class Rate {
  Rate({this.date, this.sum, this.comment});

  DateTime date;
  String sum;
  String comment;

  toMap() {
    return {'date': this.date, 'sum': this.sum, 'comment': comment};
  }
}
