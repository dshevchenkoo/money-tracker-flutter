import 'package:diplom_money_tracker/accounting/category/category.dart';
import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:diplom_money_tracker/accounting/rate/rate_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RateView extends StatelessWidget {
  const RateView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Category category = ModalRoute.of(context).settings.arguments;
    final Color color = Color(int.parse('ff${category.color}', radix: 16));

    final String categoryName = category.name;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0), // here the desired height
        child: AppBar(
            backgroundColor: color,
            automaticallyImplyLeading: false,
            centerTitle: true,
            bottom: PreferredSize(
                preferredSize: Size.fromHeight(5.0), // here the desired height
                child: Consumer<AccountingProvider>(
                    builder: (context, accountingProvider, child) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: FlatButton(
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.keyboard_arrow_left,
                                size: 40,
                              ),
                              shape: CircleBorder(
                                  side: BorderSide(color: Colors.transparent)),
                            ),
                          ),
                          Spacer(),
                          Text(
                            categoryName,
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          Spacer(),
                          Spacer(),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  );
                }))),
      ),
      body: category.rates.isEmpty
          ? Center(child: Text('Нет данных'))
          : RateList(category: category),
    );
  }
}
