import 'package:diplom_money_tracker/accounting/category/category.dart';
import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RateList extends StatelessWidget {
  const RateList({Key key, this.category}) : super(key: key);
  final Category category;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: category.rates.length,
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            key: Key(category.rates[index].date.toString()),
            confirmDismiss: (_) async {
              final rate = double.parse(category.rates[index].sum);
              bool isDelete = false;
              await showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                        title: Text("Удаление расходов"),
                        content: Text(
                            "Вы действительно хотите удалить данный расход?"),
                        actions: [
                          Consumer<AccountingProvider>(
                              builder: (context, accountingProvider, child) =>
                                  FlatButton(
                                    child: Text("Удалить"),
                                    onPressed: () {
                                      accountingProvider.deleteRate(
                                          category, category.rates[index]);
                                      Navigator.of(context).pop();
                                      isDelete = true;
                                    },
                                  )),
                          FlatButton(
                            child: Text("Отмена"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ));
              if (isDelete == true) {
                Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Расходы ${rate.toString()} ₽ удалены")));
              }
              return isDelete;
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(.5),
                      blurRadius: 5.0,
                      offset: Offset(
                        0.0,
                        5.0,
                      ),
                    )
                  ],
                ),
                child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.white,
                    child: Container(
                      height: 70,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 25.0, right: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${double.parse(category.rates[index].sum).toString()} ₽",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Consumer<AccountingProvider>(
                                    builder: (context, accountingProvider,
                                            child) =>
                                        Text(
                                          accountingProvider.getDateWithTime(
                                              category.rates[index].date),
                                          style: TextStyle(color: Colors.grey),
                                        )),
                              ],
                            ),
                            category.rates[index].comment.length < 20
                                ? Text(category.rates[index].comment)
                                : Text(
                                    "${category.rates[index].comment.substring(0, 20)}...")
                          ],
                        ),
                      ),
                    )),
              ),
            ),
          );
        });
  }
}
