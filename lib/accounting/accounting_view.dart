import 'package:diplom_money_tracker/accounting/category/category_list.dart';
import 'package:flutter/material.dart';
import 'chart/chart.dart';

class AccountingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Chart(),
          CategoryList(),
        ],
      ),
    );
  }
}
