import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diplom_money_tracker/accounting/category/add_category.dart';
import 'package:diplom_money_tracker/accounting/rate/add_rate.dart';
import 'package:diplom_money_tracker/accounting/rate/rate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import '../category/category.dart';

class AccountingProvider extends ChangeNotifier {
  DateTime selectedDate = DateTime.now();
  Color currentColor = Colors.limeAccent;

  final dbRef = FirebaseFirestore.instance;

  addCategory(Category category) async {
    await dbRef.collection('categories').add(category.toMap());
    notifyListeners();
  }

  filterRatesDate(Category category) {
    List<Rate> rates = category.rates;
    category.updateRate(rates
        .where((rate) =>
            rate.date.month == selectedDate.month &&
            rate.date.year == selectedDate.year)
        .toList());
    return category;
  }

  String getCurrentMonth() {
    List<String> month = getMonth();

    return month[selectedDate.month - 1];
  }

  List<String> getMonth() {
    return [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь',
    ];
  }

  Map<String, String> getShortMonth() {
    return {
      'Январь': 'янв',
      'Февраль': 'фев',
      'Март': 'мар',
      'Апрель': 'апр',
      'Май': 'май',
      'Июнь': 'июнь',
      'Июль': 'июль',
      'Август': 'авг',
      'Сентябрь': 'сен',
      'Октябрь': 'окт',
      'Ноябрь': 'нояб',
      'Декабрь': 'дек',
    };
  }

  String getShortDate() {
    List<String> month = getMonth();

    String currentMonth = month[selectedDate.month - 1];
    String shortCurrentMonth = (getShortMonth())[currentMonth];
    String currentYear = selectedDate.year.toString();
    String currentDay = selectedDate.day.toString();

    return '$currentDay $shortCurrentMonth $currentYear';
  }

  String getDate() {
    List<String> month = getMonth();

    String currentMonth = month[selectedDate.month - 1];
    String currentYear = selectedDate.year.toString();

    return '$currentMonth $currentYear';
  }

  selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1990),
      lastDate: DateTime(2099),
    );
    if (picked != null && picked != selectedDate) {
      selectedDate = picked;
      notifyListeners();
    }
  }

  Future<void> createCategory(context) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AddCategory();
        });
  }

  Future<void> addRateMoney(context, id) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AddRate(id: id);
        });
  }

  void addRate(id, sum, comment) async {
    final doc = dbRef.collection("categories").doc(id);

    Rate rate = Rate(sum: sum, date: selectedDate, comment: comment);
    Category categoryDb = Category().parseFromMap(await doc.get());
    categoryDb.addRate(rate);

    await doc.update(categoryDb.toMap());
    notifyListeners();
  }

  getFilteredCategories() async {
    final categoriesDb = await dbRef.collection("categories").get();

    if (categoriesDb.docs.isEmpty) {
      return [];
    }

    List<Category> categories = [];
    categoriesDb.docs.forEach((element) {
      Category category = Category().parseFromMap(element),
          filteredCategory = filterRatesDate(category);
      categories.add(filteredCategory);
    });

    return categories;
  }

  Future<List<charts.Series<ChartCategory, int>>> getSeriesList() async {
    List<dynamic> filteredCategories = [];
    filteredCategories = await getFilteredCategories();
    return createChartData(filteredCategories);
  }

  List<charts.Series<ChartCategory, int>> createChartData(List categories) {
    if (categories.every((element) => element.rates.isEmpty)) {
      return [];
    }

    List<ChartCategory> chartCategories = [];
    categories.asMap().forEach((key, element) {
      chartCategories.add(ChartCategory(id: key, category: element));
    });

    return [
      new charts.Series<ChartCategory, int>(
          id: 'Category',
          domainFn: (ChartCategory row, _) => row.id,
          measureFn: (ChartCategory row, _) => row.category.rates.fold(
              0, (previous, current) => previous + double.parse(current.sum)),
          data: chartCategories,
          labelAccessorFn: (ChartCategory row, _) => row.category.name,
          colorFn: (ChartCategory row, _) {
            Color currentColor =
                Color(int.parse('ff${row.category.color}', radix: 16));
            return charts.Color(
                r: currentColor.red,
                g: currentColor.green,
                b: currentColor.blue);
          })
    ];
  }

  deleteCategory(id) async {
    await dbRef.collection("categories").doc(id).delete();
    notifyListeners();
  }

  String getDateWithTime(DateTime date) {
    final month = getMonth()[date.month - 1].toLowerCase();
    return "${date.day} $month ${date.year} ${date.hour}:${date.minute}";
  }

  void deleteRate(Category category, Rate rate) async {
    final categories = await dbRef.collection("categories").get();
    final categoryFromDb = categories.docs.where((element) =>
        element.data()['name'] == category.name &&
        element.data()['color'] == category.color);

    final categoryId = categoryFromDb.first.id;

    final Category categoryDb =
        Category().parseFromMap(categoryFromDb.first.data());

    final ratesDb = categoryDb.rates;

    int deleteIndex = -1;
    ratesDb.asMap().forEach((key, value) {
      if (value.sum == rate.sum &&
          value.comment == rate.comment &&
          value.date == rate.date) {
        deleteIndex = key;
      }
    });

    if (deleteIndex == -1) {
      return;
    }

    ratesDb.removeAt(deleteIndex);
    final Category updateCategory = categoryDb.updateRate(ratesDb);

    final doc = dbRef.collection("categories").doc(categoryId);
    await doc.update(updateCategory.toMap());

    notifyListeners();
  }
}

class ChartCategory {
  final int id;
  final Category category;

  ChartCategory({this.id, this.category});
}
