import 'package:charts_flutter/flutter.dart';
import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class Chart extends StatelessWidget {
  const Chart({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Consumer<AccountingProvider>(
          builder: (context, accountingProvider, child) {
        return FutureBuilder<List<Series<ChartCategory, int>>>(
          future: accountingProvider.getSeriesList(), // async work
          builder: (BuildContext context,
              AsyncSnapshot<List<Series<ChartCategory, int>>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Text('Загрузка ...');
              case ConnectionState.none:
                return Center(
                  child: Text(
                      'За ${accountingProvider.getCurrentMonth()} нет данных'),
                );
              default:
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                        'За ${accountingProvider.getCurrentMonth()} нет данных'),
                  );
                } else {
                  return Center(
                      child: snapshot.data.isEmpty
                          ? Text(
                              'За ${accountingProvider.getCurrentMonth()} нет данных')
                          : PieChart(snapshot.data,
                              animate: false,
                              defaultRenderer: charts.ArcRendererConfig(
                                  arcWidth: 60,
                                  arcRendererDecorators: [
                                    charts.ArcLabelDecorator()
                                  ])));
                }
            }
          },
        );
      }),
    );
  }
}
