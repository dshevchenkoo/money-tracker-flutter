import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';

import './category.dart';

class AddCategory extends StatefulWidget {
  @override
  _AddCategoryState createState() => _AddCategoryState();
}

class _AddCategoryState extends State<AddCategory> {
  void changeColor(Color color) => {
        currentColor = color,
        colorString = currentColor.toString().split('(0xff')[1].split(')')[0],
        colorController.text = colorString
      };

  Color currentColor = Colors.limeAccent;
  String colorString = '';

  bool _validName = true;
  bool _validColor = true;

  TextEditingController nameController = TextEditingController();
  TextEditingController colorController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      title: Center(child: Text('Добавить категорию')),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                  labelText: 'Название',
                  errorText: !_validName ? 'Введите название' : null),
            ),
            TextField(
              maxLength: 6,
              controller: colorController,
              decoration: InputDecoration(
                  errorText: !_validColor ? 'Введите или выберете цвет' : null,
                  labelText: 'Цвет',
                  suffixIcon: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Выберите цвет'),
                              content: SingleChildScrollView(
                                child: BlockPicker(
                                  pickerColor: currentColor,
                                  onColorChanged: changeColor,
                                ),
                              ),
                            );
                          },
                        );
                      },
                      child: Icon(Icons.color_lens))),
            ),
            SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 45,
              child: Consumer<AccountingProvider>(
                  builder: (context, accountingProvider, child) => RaisedButton(
                        color: Color.fromRGBO(144, 23, 255, 100),
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          'Добавить',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.normal),
                        ),
                        onPressed: () {
                          setState(() {
                            nameController.text.isEmpty
                                ? _validName = false
                                : _validName = true;

                            colorController.text.isEmpty
                                ? _validColor = false
                                : _validColor = true;
                          });

                          if (!_validColor || !_validName) {
                            return;
                          }
                          Category category = Category(
                              color: colorString, name: nameController.text);
                          accountingProvider.addCategory(category);

                          Navigator.of(context).pop();
                        },
                      )),
            ),
            CupertinoButton(
                child: Text(
                  'Отмена',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
      ),
    );
  }
}
