import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diplom_money_tracker/accounting/category/category.dart';
import 'package:diplom_money_tracker/accounting/provider/accounting_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryList extends StatelessWidget {
  const CategoryList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double getRatesSum(Category category) {
      double rateSum = 0;
      category.rates.forEach((element) {
        rateSum = rateSum + double.parse(element.sum);
      });

      return rateSum;
    }

    return Expanded(
        child: StreamBuilder(
            stream:
                FirebaseFirestore.instance.collection('categories').snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshots) {
              if (snapshots.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              }
              List items = snapshots.data?.docs ?? [];

              if (items.isEmpty) {
                return Text('Добавьте категорию');
              }

              return ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (BuildContext context, int index) {
                    Category currentCategory =
                        Category().parseFromMap(items[index]);

                    String currentName = currentCategory.name;
                    String currentColor = currentCategory.color;

                    int colorInt = int.parse('ff$currentColor', radix: 16);

                    return Consumer<AccountingProvider>(
                        builder: (context, accountingProvider, child) {
                      currentCategory =
                          accountingProvider.filterRatesDate(currentCategory);
                      double rateSum = getRatesSum(currentCategory);

                      return Dismissible(
                        key: Key(currentName),
                        confirmDismiss: (_) async {
                          bool isDelete = false;
                          await showDialog(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                    title: Text("Удаление категории"),
                                    content: Text(
                                        "Вы действительно хотите удалить категорию? Все данные о расходах данной категории будут удалены."),
                                    actions: [
                                      FlatButton(
                                        child: Text("Удалить"),
                                        onPressed: () {
                                          accountingProvider
                                              .deleteCategory(items[index].id);
                                          Navigator.of(context).pop();
                                          isDelete = true;
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Отмена"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  ));
                          if (isDelete == true) {
                            Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text("$currentName удален")));
                          }
                          return isDelete;
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(.5),
                                    blurRadius: 5.0,
                                    offset: Offset(
                                      0.0,
                                      5.0,
                                    ),
                                  )
                                ],
                              ),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                color: Colors.white,
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(10),
                                  onTap: () {
                                    return accountingProvider.addRateMoney(
                                        context, items[index].id);
                                  },
                                  child: Container(
                                    height: 70,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, right: 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(currentName),
                                              SizedBox(height: 5),
                                              Text(
                                                  rateSum == 0 &&
                                                          currentCategory.rates
                                                                  .length ==
                                                              0
                                                      ? 'Добавить расход'
                                                      : rateSum.toString(),
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 12))
                                            ],
                                          ),
                                          InkWell(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            child: Icon(
                                              Icons.navigate_next,
                                              color: Color(colorInt),
                                              size: 35,
                                            ),
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context, '/rate',
                                                  arguments: currentCategory);
                                            },
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                        ),
                      );
                    });
                  });
            }));
  }
}
