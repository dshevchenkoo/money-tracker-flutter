import 'package:diplom_money_tracker/accounting/rate/rate.dart';

class Category {
  Category({this.color, this.name});

  String color;
  String name;

  List<Rate> rates = [];
  addRate(Rate rate) {
    rates.add(rate);
  }

  toMap() {
    List rates = [];
    this.rates.forEach((rate) {
      rates.add(rate.toMap());
    });

    return {'name': this.name, 'color': this.color, 'rates': rates};
  }

  updateRate(List<Rate> rates) {
    this.rates = rates;
    return this;
  }

  parseFromMap(mapData) {
    this.color = mapData['color'];
    this.name = mapData['name'];

    List<Rate> rates = [];

    mapData['rates'].forEach((rate) => {
          rates.add(Rate(
              comment: rate['comment'] ?? '',
              date: DateTime.fromMillisecondsSinceEpoch(
                  rate['date'].seconds * 1000),
              sum: rate['sum'] ?? 0))
        });

    this.rates = rates;
    return this;
  }
}
