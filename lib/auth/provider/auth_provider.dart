import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:image_picker/image_picker.dart';

enum AuthState { isSignedIn, isSignedUp }

class AuthProvider extends ChangeNotifier {
  final picker = ImagePicker();

  bool isValid = false;

  bool emailValid = true;
  bool passwordValid = true;
  bool secondPasswordValid = true;
  bool userIsAuth = false;

  final auth.FirebaseAuth firebaseAuth = auth.FirebaseAuth.instance;

  void resetValidParams() {
    isValid = false;
    emailValid = true;
    passwordValid = true;
    secondPasswordValid = true;
  }

  void validate(Map<String, TextEditingController> controllers) {
    resetValidParams();

    if (controllers['email'].text.isEmpty) {
      emailValid = false;
    }
    if (controllers['password'].text.isEmpty) {
      passwordValid = false;
    }

    if (controllers['secondPassword'] != null &&
        controllers['password'].text.isEmpty) {
      secondPasswordValid = false;
    }

    if (emailValid && passwordValid && secondPasswordValid) {
      isValid = true;
    }

    notifyListeners();
  }

  void doAuthAction(context, Map<String, TextEditingController> controllers) {
    if (!isValid) return;

    int length = controllers.length;
    String email = controllers['email'].text;
    String password = controllers['password'].text;
    if (length == 2) {
      signIn(email, password);
    } else {
      signUp(email, password);
      Navigator.pushReplacementNamed(context, '/security');
    }
    notifyListeners();
  }

  Future<String> signUp(String email, String password) async {
    try {
      await firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> signIn(String email, String password) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  auth.User getCurrentUser() {
    auth.User user = firebaseAuth.currentUser;
    return user;
  }

  Future<void> signOut(context) async {
    await firebaseAuth.signOut();
  }

  Future<String> getEmail() async {
    final auth.User user = await this.getCurrentUser();
    return user.email;
  }

  isAuth() async {
    final auth.User user = await this.getCurrentUser();
    if (user == null || user.isAnonymous) {
      return false;
    } else {
      return true;
    }
  }

  saveImage(context, image) async {
    final auth.User user = await this.getCurrentUser();

    final Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('users')
        .child('/${user.uid}')
        .child('/avatar.jpg');
    final UploadTask uploadTask = storageReference.putFile(image);
    TaskSnapshot taskSnapshot = await Future.value(uploadTask);
    if (taskSnapshot != null) {
      String downloadUrl = await taskSnapshot.ref.getDownloadURL();
      await auth.FirebaseAuth.instance.currentUser
          .updateProfile(photoURL: downloadUrl);
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
        'Изображение установлено',
      )));
    }
  }

  Future setImage(context) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      File image = File(pickedFile.path);
      saveImage(context, image);
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
        'Ошибка!\n Изображение не выбрано',
      )));
    }
  }

  getImage() async {
    final auth.User user = await this.getCurrentUser();
    if (user == null || user.isAnonymous) {
      return false;
    } else {
      return user.photoURL;
    }
  }
}
