import 'package:flutter/material.dart';
import 'common/info_header.dart';
import 'common/quest_button.dart';
import 'common/login_button.dart';
import 'common/login_field.dart';
import 'service/controller.dart';
import 'package:provider/provider.dart';
import 'provider/auth_provider.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    final Color myColor = Color.fromRGBO(144, 23, 255, 100);
    final emailTextController = TextEditingController();
    final passwordTextController = TextEditingController();
    final secondPasswordTextController = TextEditingController();

    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(child: Info()),
              Expanded(
                  child: SingleChildScrollView(
                child: Consumer<AuthProvider>(
                    builder: (context, authProvider, child) => Column(
                          children: [
                            LoginField(
                              color: myColor,
                              labelName: 'E-mail',
                              textController: emailTextController,
                              valid: authProvider.emailValid,
                            ),
                            LoginField(
                              color: myColor,
                              labelName: 'Пароль',
                              textController: passwordTextController,
                              valid: authProvider.passwordValid,
                            ),
                            LoginField(
                              color: myColor,
                              labelName: 'Повторный пароль',
                              textController: secondPasswordTextController,
                              valid: authProvider.secondPasswordValid,
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            LoginButton(
                              color: myColor,
                              buttonName: 'Регистрация',
                              controllers: {
                                'email': emailTextController,
                                'password': passwordTextController,
                                'secondPassword': secondPasswordTextController
                              },
                            ),
                          ],
                        )),
              )),
              QuestButton(
                question: 'Уже есть аккаунт?',
                buttonName: 'Войти',
                onPressed: navigateToSignIn(context),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
