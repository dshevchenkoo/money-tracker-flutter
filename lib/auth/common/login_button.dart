import 'package:diplom_money_tracker/auth/provider/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginButton extends StatelessWidget {
  final Color color;
  final String buttonName;
  final Map<String, TextEditingController> controllers;

  const LoginButton({Key key, this.color, this.buttonName, this.controllers})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 45,
      child: Consumer<AuthProvider>(
          builder: (context, authProvider, child) => RaisedButton(
              color: color,
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0),
              ),
              child: Text(
                buttonName,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                authProvider.validate(controllers);
                authProvider.doAuthAction(context, controllers);
              })),
    );
  }
}
