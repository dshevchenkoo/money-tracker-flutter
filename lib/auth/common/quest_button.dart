import 'package:flutter/material.dart';

class QuestButton extends StatelessWidget {
  final String buttonName;
  final String question;
  final Function onPressed;
  const QuestButton({Key key, this.question, this.buttonName, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(question),
        FlatButton(
          child: Text(
            buttonName,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(144, 23, 255, 100)),
          ),
          onPressed: onPressed,
        )
      ],
    );
  }
}
