import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  final image = Image.asset(
    'assets/images/logo.png',
  );
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 70,
        ),
        Center(child: SizedBox(width: 70, child: image)),
        SizedBox(
          height: 10,
        ),
        Text(
          'Учёт расходов',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
        ),
        SizedBox(
          height: 15,
        ),
        Text(
          'Ваша история расходов',
          style: TextStyle(fontSize: 18),
        ),
        Text(
          'всегда под рукой',
          style: TextStyle(fontSize: 18),
        ),
      ],
    );
  }
}
