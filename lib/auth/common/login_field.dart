import 'package:flutter/material.dart';

class LoginField extends StatelessWidget {
  final String labelName;
  final Color color;
  final TextEditingController textController;
  final bool valid;

  LoginField({this.labelName, this.color, this.textController, this.valid});
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(
          primaryColor: color,
        ),
        child: TextField(
          controller: textController,
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: color),
              ),
              errorText: !valid && textController.text.isEmpty
                  ? 'Введите $labelName'
                  : null,
              labelText: labelName),
        ));
  }
}
