import 'package:diplom_money_tracker/auth/provider/auth_provider.dart';
import 'package:flutter/material.dart';
import 'common/info_header.dart';
import 'common/quest_button.dart';
import 'common/login_button.dart';
import 'common/login_field.dart';
import 'service/controller.dart';
import 'package:provider/provider.dart';

class SignIn extends StatefulWidget {
  SignIn({Key key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  @override
  Widget build(BuildContext context) {
    final Color myColor = Color.fromRGBO(144, 23, 255, 100);
    final emailTextController = TextEditingController();
    final passwordTextController = TextEditingController();

    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Expanded(child: Info()),
              Expanded(
                  child: Consumer<AuthProvider>(
                      builder: (context, authProvider, child) => Column(
                            children: [
                              LoginField(
                                  color: myColor,
                                  labelName: 'E-mail',
                                  textController: emailTextController,
                                  valid: authProvider.emailValid),
                              LoginField(
                                  color: myColor,
                                  labelName: 'Пароль',
                                  textController: passwordTextController,
                                  valid: authProvider.passwordValid),
                              SizedBox(
                                height: 30,
                              ),
                              LoginButton(
                                color: myColor,
                                buttonName: 'Войти',
                                controllers: {
                                  'email': emailTextController,
                                  'password': passwordTextController
                                },
                              ),
                            ],
                          ))),
              QuestButton(
                question: 'Ещё нет аккаунта?',
                buttonName: 'Регистрация',
                onPressed: navigateToSignUp(context),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
