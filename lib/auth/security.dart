import 'package:diplom_money_tracker/auth/provider/auth_provider.dart';
import 'package:diplom_money_tracker/auth/sign_in_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app_view.dart';

class Security extends StatelessWidget {
  const Security({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthProvider>(
        builder: (context, authProvider, child) => StreamBuilder(
            stream: authProvider.firebaseAuth.authStateChanges(),
            builder: (context, snapshot) {
              return snapshot.hasData ? AppView() : SignIn();
            }));
  }
}
