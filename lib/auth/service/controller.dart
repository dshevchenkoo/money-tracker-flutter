import 'package:flutter/material.dart';

navigateToSignUp(context) {
  return () {
    Navigator.pushNamed(context, '/login/sign-up');
  };
}

navigateToSignIn(context) {
  return () {
    Navigator.pushNamed(context, '/login/sign-in');
  };
}

navigateToAccountingView(context) {
  return () {
    Navigator.pushNamed(context, '/accounting');
  };
}
