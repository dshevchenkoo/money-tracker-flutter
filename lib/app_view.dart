import 'package:diplom_money_tracker/accounting/accounting_view.dart';
import 'package:diplom_money_tracker/profile/profile_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'accounting/provider/accounting_provider.dart';

class AppView extends StatefulWidget {
  AppView({Key key}) : super(key: key);

  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final Color myColor = Color.fromRGBO(144, 23, 255, 100);

  int _selectedIndex = 0;

  List _widgetOptions = <Widget>[
    Consumer<AccountingProvider>(
        builder: (context, accountingProvider, child) => AccountingView()),
    ProfileView()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: AppBar(
            backgroundColor: myColor,
            automaticallyImplyLeading: false,
            centerTitle: true,
            bottom: PreferredSize(
                preferredSize: Size.fromHeight(5.0), // here the desired height
                child: Consumer<AccountingProvider>(
                    builder: (context, accountingProvider, child) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _selectedIndex == 1
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Text('Профиль',
                                  style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold)),
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Spacer(),
                                InkWell(
                                  onTap: () {
                                    accountingProvider.selectDate(context);
                                  },
                                  child: Text(
                                    accountingProvider.getDate(),
                                    style: TextStyle(
                                        fontSize: 25,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Expanded(
                                  child: FlatButton(
                                    textColor: Colors.white,
                                    onPressed: () {
                                      accountingProvider
                                          .createCategory(context);
                                    },
                                    child: Icon(
                                      Icons.add,
                                    ),
                                    shape: CircleBorder(
                                        side: BorderSide(
                                            color: Colors.transparent)),
                                  ),
                                ),
                              ],
                            ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  );
                }))),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.credit_card),
            label: 'Расходы',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Профиль',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
