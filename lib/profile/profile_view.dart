import 'package:diplom_money_tracker/auth/provider/auth_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 20),
      child: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              Consumer<AuthProvider>(
                  builder: (context, authProvider, child) => InkWell(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        onTap: () {
                          authProvider.setImage(context);
                        },
                        child: CircleAvatar(
                            radius: 50.0,
                            child: FutureBuilder(
                              future: authProvider.getImage(),
                              builder: (BuildContext context, snapshot) {
                                print(snapshot.data);
                                if (snapshot.hasData == false) {
                                  return Icon(Icons.photo);
                                } else if (snapshot.hasData == true) {
                                  return ClipOval(
                                      child: Image.network(snapshot.data));
                                }
                              },
                            )),
                      )),
            ],
          ),
          Column(
            children: [
              Consumer<AuthProvider>(
                  builder: (context, authProvider, child) =>
                      FutureBuilder<String>(
                          future: authProvider.getEmail(),
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            if (snapshot.hasData) {
                              return Text(
                                snapshot.data,
                                style: TextStyle(fontSize: 18),
                              );
                            } else {
                              return CircularProgressIndicator();
                            }
                          })),
              SizedBox(
                height: 10,
              ),
              Consumer<AuthProvider>(
                  builder: (context, authProvider, child) => SizedBox(
                        width: 150,
                        height: 50,
                        child: RaisedButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(15.0),
                            ),
                            color: Color.fromRGBO(144, 23, 255, 100),
                            child: Text(
                              'Выйти',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {
                              authProvider.signOut(context);
                            }),
                      ))
            ],
          )
        ],
      )),
    );
  }
}
